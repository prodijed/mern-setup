let express   		   = require("express"),
    router      	   = express.Router(),
    passport           = require("passport"),
	bodyParser 		   = require("body-parser"),
    createError        = require('http-errors'),
    // Use user in model so that same in login
    UserRegistration   = require('../models/user'),
    DATE               = require('../controllers/date'),
    controllerUserRegistration = require('../controllers/user_registration');
    // controllerIndex    = require('../controller/index');

// LOGIN POST ROUTE
// load region
router.get('/l/region', controllerUserRegistration.get_region);
router.get('/l/province', controllerUserRegistration.get_province);
router.get('/l/city', controllerUserRegistration.get_city);
router.get('/l/district', controllerUserRegistration.get_district);
router.get('/l/barangay', controllerUserRegistration.get_barangay);

router.get('/g/record_user', controllerUserRegistration.get_record_user);
router.get('/g/record', controllerUserRegistration.get_record);
// router.post('/create/record', controllerUserRegistration.create_record);
router.post('/u/record', controllerUserRegistration.update_record);
router.post('/d/record', controllerUserRegistration.delete_record)

module.exports = router;
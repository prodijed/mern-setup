const User = require("../models/user");
const jwt = require("jsonwebtoken");
require("dotenv").config();

exports.register = async (req, res) => {
	// check if user already exists
	const usernameExists = await User.findOne({
		username: req.body.username,
	});
	const emailExists = await User.findOne({
		email: req.body.email,
	});

	if (usernameExists) {
		return res.status(403).json({
			error: "Username is taken",
		});
	}
	if (emailExists) {
		return res.status(403).json({
			error: "Email is taken",
		});
	}

	// if new user, create a new user
	const user = new User(req.body);
	await user.save();

	res.status(201).json({
		message: "Signup Successful! Please Login to proceed",
	});
};

exports.login = async (req, res) => {
	const { email, password } = req.body;

	try {
		// Find the user based on email
		const user = await User.findOne({ email });

		if (!user) {
			return res.status(401).json({
				error: "Invalid Credentials",
			});
		}
		// const region_code = x.region;
		// const province_code = x.province;
		// const city_code = x.city;
		// const district_code = x.district;
		// const barangay_code = x.barangay;

		// Use the user.authenticate method to verify the password
		if (!user.authenticate(password)) {
			return res.status(401).json({
				error: "Invalid email or password",
			});
		}

		// Generate a token with user id and jwt secret
		const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, {
			expiresIn: "24h",
		});

		// Set the JWT token as a cookie with an expiry date
		res.cookie("jwt", token, { expires: new Date(Date.now() + 9999), httpOnly: true });

		// Return the response with user
		const { username } = user;
		const { region } = user;
		const { province } = user;
		const { city } = user;
		const { district } = user;
		const { barangay } = user
		
		return res.json({
			message: "Login Successful!",
			username,
			region,
			province,
			city,
			district,
			barangay,
		});
	} catch (err) {
		console.error(err);
		return res.status(500).json({
			error: "Internal Server Error",
		});
	}
};

exports.logout = (req, res) => {
	// clear the cookie
	res.clearCookie("jwt");

	return res.json({
		message: "Logout Successful!",
	});
};

exports.getLoggedInUser = (req, res) => {
	const { username } = req.user;

	return res.status(200).json({
		message: "User is still logged in",
		username,
	});
};
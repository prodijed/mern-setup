let express = require("express"),
    router = express.Router(),
    passport = require("passport"),
    bodyParser = require("body-parser"),
    createError = require('http-errors'),
    // JWT                = require('jsonwebtoken'),
    Region = require('../models/region'),
    Province = require('../models/province'),
    City = require('../models/city'),
    District = require('../models/district'),
    Barangay = require('../models/barangay'),
    // UserRegistration = require('../models/user_registration'),
    UserRegistration = require('../models/user'),
    DATE = require('./date');

// const url = require('url');
// get access to URLSearchParams object
// const search_params = current_url.searchParams;
// console.log('params=' + search_parms);

const get_region = async (req, res) => {
    try {
        // const x = await Region.find({status:true}).select('sequence code description dateModifiedTime order by sequence')
        const x = await Region.find({ status: true }).sort({ sequence: 1 })
        if (!x) throw createError(403, 'Not found!');
        console.log(x)
        res.send(x)
    } catch (e) {
        res.send({ error: 'Something went wrong, Please try again' })
    }
}

const get_province = async (req, res) => {
    try {
        const region_code = req.query.region_code;
        // const x = await Region.find({status:true}).select('sequence code description dateModifiedTime order by sequence')
        const x = await Province.find({ region: region_code, status: true }).sort({ region: 1, code: 1 })
        if (!x) throw createError(403, 'Not found!');
        console.log(x)
        res.send(x)
    } catch (e) {
        res.send({ error: 'Something went wrong, Please try again' })
    }
}


const get_city = async (req, res) => {
    try {
        const region_code = req.query.region_code;
        const province_code = req.query.province_code;
        const x = await City.find({ region: region_code, province: province_code, status: true }).sort({ region: 1, province: 1, city: 1 })
        if (!x) throw createError(403, 'Not found!');
        console.log(x)
        res.send(x)
    } catch (e) {
        res.send({ error: 'Something went wrong, Please try again' })
    }
}

const get_district = async (req, res) => {
    try {
        const region_code = req.query.region_code;
        const province_code = req.query.province_code;
        const city_code = req.query.city_code;
        const x = await District.find({ region: region_code, province: province_code, city: city_code, status: true }).sort({ region: 1, province: 1, city: 1, district: 1  })
        if (!x) throw createError(403, 'Not found!');
        console.log(x)
        res.send(x)
    } catch (e) {
        res.send({ error: 'Something went wrong, Please try again' })
    }
}

const get_barangay = async (req, res) => {
    try {
        const region_code = req.query.region_code;
        const province_code = req.query.province_code;
        const city_code = req.query.city_code;
        const district_code = req.query.district_code;
        const x = await Barangay.find({ region: region_code, province: province_code, city: city_code, district: district_code, status: true }).sort({ region: 1, province: 1, city: 1, district: 1, barangay: 1  })
        if (!x) throw createError(403, 'Not found!');
        console.log(x)
        res.send(x)
    } catch (e) {
        res.send({ error: 'Something went wrong, Please try again' })
    }
}

/*
const create_record = async (req, res) => {
    let actionFB = 'Added';
    try {
        console.log('create_record ------------------ ');

        const region = req.sanitize(req.body.region);       const province = req.sanitize(req.body.province);
        const city = req.sanitize(req.body.city);           const district = req.sanitize(req.body.district);
        const barangay = req.sanitize(req.body.barangay);   const username = req.sanitize(req.body.username);
        const password = req.sanitize(req.body.password);   const firstName = req.sanitize(req.body.firstName);
        const lastName = req.sanitize(req.body.lastName);  
        const company = '';
        const address = req.sanitize(req.body.address);     const contact = req.sanitize(req.body.contact);
        const role = '';                
        const isSu = false;                             const isFirstLoggedIn = true;   
        const resetPasswordToken = '';                  const resetPasswordExpires = 1;
        const licenseNumber = '';                       const licenseExpiration = '';   
        const station = '';                             const token = '';
        const createdBy = 'Lex';
        const modifiedBy = 'Lex';     
        const dateApproval = '';                        const validationToken = '';
        const OTPExpiration = '';                       const otp = '';

        // dateCreated: DATE.dateOnly(),
        console.log('r=' + region + ' p=' + province + ' c=' + city + ' d=' + district + ' barangay=' + barangay + ' username=' + username);
        const details = {
            region: region, province: province, city: city, district: district, barangay: barangay,
            username: username, password: password, firstName: firstName, lastName: lastName,
            company: company, contact: contact, address: address, role: role,
            isSu: isSu, isFirstLoggedIn: isFirstLoggedIn, resetPasswordToken: resetPasswordToken,
            resetPasswordExpires: resetPasswordExpires, licenseNumber: licenseNumber,
            licenseExpiration: licenseExpiration, station: station, token: token,
            dateCreatedTime: DATE.dateWithTime(), createdBy: createdBy,
            dateModifiedTime: DATE.dateWithTime(), modifiedBy: modifiedBy,
            dateApproval: dateApproval, validationToken: validationToken,
            OTPExpiration: OTPExpiration, otp:otp,


        }
        console.log('Backend =======' + JSON.stringify(details))
        const userIsExist = await UserRegistration.findOne({ username: username,  status: 1 });
        console.log('reg=' + region + ' prov=' + province + ' c=' + city + ' d=' + district + ' userIsExist=' + userIsExist);
        if (userIsExist) {
            actionFB = 'Duplicate';

            throw createError(403, `User name ${username} already exist!`)
        }


        //const codeIsExist = await Province.findOne({code: code});
        //if(codeIsExist) throw createError(403, `Code ${code} already exist!`);
        console.log('Username  ready to save...')
        const newUserRegistration = new UserRegistration(details);

        // JOM const x = await Region.register(newUser, password);
        const x = await newUserRegistration.save();
        if (!x) throw createError(403, 'Something went wrong while creating');
        console.log(x)
        actionFB = 'Created';
        res.send({ actionFB: actionFB, success: `Successfully Created` })
    } catch (e) {
        res.send({ actionFB: actionFB, error: e.message })
    }
}
*/


const get_record = async (req, res) => {
    try {
        // const x = await Region.find({status:true}).select('sequence code description dateModifiedTime order by sequence')
        const x = await Barangay.find({ status: true }).sort({ province: -1 })

        if (!x) throw createError(403, 'Not found!');
        console.log(x)
        res.send(x)
    } catch (e) {
        res.send({ error: 'Something went wrong, Please try again' })
    }
}


const get_record_user = async (req, res) => {
    try {

        console.log('user req get record user = ' + JSON.stringify(req.query));
        const region_code = req.query.region_code;
        const province_code = req.query.province_code;
        const city_code = req.query.city_code;
        const district_code = req.query.district_code;
        const barangay_code = req.query.barangay_code;
        let matching_values = {};
        // if (region_code === "All" && province_code === 'All') {
        // if (1 === 1){    
        console.log('Pasok ALL ALL ALL')
        if (region_code === "All" && province_code === 'All' && city_code === 'All' && district_code === 'All') {       // 0000
            matching_values = { status: 1 }
        } else if (region_code === "All" && province_code === 'All' && city_code === 'All' && district_code != 'All') { // 0001                 
            matching_values = { district: district_code, status: 1 }
        } else if (region_code === "All" && province_code === 'All' && city_code != 'All' && district_code === 'All') { // 0010 
            matching_values = { city: city_code, status: 1 }
        } else if (region_code === "All" && province_code === 'All' && city_code != 'All' && district_code != 'All') {  // 0011 
            matching_values = { city: city_code, district: district_code, status: 1 }

        } else if (region_code === "All" && province_code != 'All' && city_code === 'All' && district_code === 'All') {  // 0100 
            matching_values = { province: province_code, status: 1 }
        } else if (region_code === "All" && province_code != 'All' && city_code === 'All' && district_code != 'All') {   // 0101 
            matching_values = { province: province_code, district: district_code, status: 1 }
        } else if (region_code === "All" && province_code != 'All' && city_code != 'All' && district_code === 'All') {   // 0110 
            matching_values = { province: province_code, city: city_code, status: 1 }
        } else if (region_code === "All" && province_code != 'All' && city_code != 'All' && district_code != 'All') {   // 0111 
            matching_values = { province: province_code, city: city_code, district: district_code, status: 1 }

        } else if (region_code != "All" && province_code === 'All' && city_code === 'All' && district_code === 'All') {   // 1000 
            matching_values = { region: region_code, status: 1 }
        } else if (region_code != "All" && province_code === 'All' && city_code === 'All' && district_code != 'All') {   // 1001 
            matching_values = { region: region_code, district: district_code, status: 1 }
        } else if (region_code != "All" && province_code === 'All' && city_code != 'All' && district_code === 'All') {   // 1010 
            matching_values = { region: region_code, city: city_code, status: 1 }
        } else if (region_code != "All" && province_code === 'All' && city_code != 'All' && district_code != 'All') {   // 1011 
            matching_values = { region: region_code, city: city_code, district: district_code, status: 1 }
            
        } else if (region_code != "All" && province_code != 'All' && city_code === 'All' && district_code === 'All') {   // 1100 
            matching_values = { region: region_code, province: province_code, status: 1 }
        } else if (region_code != "All" && province_code != 'All' && city_code === 'All' && district_code !== 'All') {   // 1101 
            matching_values = { region: region_code, province: province_code, district: district_code, status: 1 }
        } else if (region_code != "All" && province_code != 'All' && city_code != 'All' && district_code === 'All') {   // 1110 
            matching_values = { region: region_code, province: province_code, city: city_code, status: 1 }                                    
        } else {  //1111   
            // console.log('Pasok not equal') 
            matching_values = { region: region_code, province: province_code, city: city_code, district: district_code, status: 1 }
        }
        // console.log('matching_values=' + JSON.stringify(matching_values));
        matching_values = { status: 1}
        console.log('matching_values=' + JSON.stringify(matching_values));
        const x = await UserRegistration.aggregate([
            { $match: matching_values },
            // { $match: { status: 1 } },
            // { $match: { region: region_code, province: province_code, city: city_code, status: 1 } },
            // { $match: { region: 0, province: 0, city: 0, status: 1 } },       
            { $sort: { region: 1, province: 1, city: 1, district: 1, barangay: 1, code: 1 } },
            {
                $lookup: {
                    from: "cities",
                    let: { user_region: "$region", user_province: "$province", user_city: "$city", user_status: "$status" },
                    pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ["$region", "$$user_region"] },
                                    { $eq: ["$province", "$$user_province"] },
                                    { $eq: ["$code", "$$user_city"] },
                                    { $eq: ["$status", "$$user_status"] }
                                ]
                            }
                        }
                    },
                    {
                        $project: {
                            _id: 0, status_sync: 0, status: 0, dateCreatedTime: 0,
                            createdBy: 0, dateModifiedTime: 0, modifiedBy: 0
                        }
                    }
                    ],
                    as: "city_name"
                }
            },
            {
                $lookup: {
                    from: "districts",
                    let: { user_region: "$region", user_province: "$province", user_city: "$city", user_district: "$district", user_status: "$status" },
                    pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ["$region", "$$user_region"] },
                                    { $eq: ["$province", "$$user_province"] },
                                    { $eq: ["$city", "$$user_city"] },
                                    { $eq: ["$code", "$$user_district"] },
                                    { $eq: ["$status", "$$user_status"] }
                                ]
                            }
                        }
                    },
                    {
                        $project: {
                            _id: 0, status_sync: 0, status: 0, dateCreatedTime: 0,
                            createdBy: 0, dateModifiedTime: 0, modifiedBy: 0
                        }
                    }
                    ],
                    as: "district_name"
                }
            },      
            {
                $lookup: {
                    from: "barangays",
                    let: { user_region: "$region", user_province: "$province", user_city: "$city", user_district: "$district", user_barangay: "$barangay", user_status: "$status" },
                    pipeline: [{
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ["$region", "$$user_region"] },
                                    { $eq: ["$province", "$$user_province"] },
                                    { $eq: ["$city", "$$user_city"] },
                                    { $eq: ["$district", "$$user_district"] },
                                    { $eq: ["$code", "$$user_barangay"] },
                                    { $eq: ["$status", "$$user_status"] }
                                ]
                            }
                        }
                    },
                    {
                        $project: {
                            _id: 0, status_sync: 0, status: 0, dateCreatedTime: 0,
                            createdBy: 0, dateModifiedTime: 0, modifiedBy: 0
                        }
                    }
                    ],
                    as: "barangay_name"
                }
            },              
            {
                $project: {
                    region: 1,
                    province: 1,
                    city: 1,
                    city_name: '$city_name.description',
                    district: 1,
                    district_name: '$district_name.description',
                    barangay: 1,
                    barangay_name: '$barangay_name.description',
                    email: 1,
                    firstName: 1,
                    lastName: 1,
                    contact: 1,
                    address: 1,
                    dateModifiedTime: 1
                }
            }

        ]);

        console.log('Tapos')
        if (!x) {
            console.log('Null');
            throw createError(403, 'Not found!');
        } else {
            res.send(x)
            console.log('Success x' + JSON.stringify(x))
        }

    } catch (e) {
        res.send({ error: 'Something went wrong, Please try again' })
    }
}



const update_record = async (req, res) => {
    try {
        console.log('create_record ------------------ ');

        const region_old = req.query.region_old;
        const province_old = req.query.province_old;
        const city_old = req.query.city_old;
        const district_old = req.query.district_old;
        const barangay_old = req.query.barangay_old;
        const email_old = req.query.email_old;
        console.log('Update brgy region_old=' + region_old + ' prov_old=' + province_old + ' city_old=' + city_old + ' dist_old=' + district_old + ' barangay_old=' + barangay_old + ' email=' + email_old)

        const region    = req.body.region;
        const province  = req.body.province;
        const city      = req.body.city;
        const district  = req.body.district;
        const barangay  = req.body.barangay;
        const email     = req.body.email;
        const firstName = req.body.firstName;
        const lastName  = req.body.lastName;
        const contact   = req.body.contact;
        const address   = req.body.address;
        const accesslevel = req.body.accesslevel;
        const modifiedby = req.body.modifiedBy;
        console.log(' values to edit=' + region + ' prov=' + province + ' city=' + city + ' district=' + district + ' barangay=' + barangay + ' email=' + email)

        const x = await UserRegistration.findOne({ region: region_old, province: province_old, city: city_old, district: district_old, barangay: barangay_old, email: email_old, status: 1 });
        if (!x) throw createError(403, `Code not found!`);
        x.region    = region;
        x.province  = province;
        x.city      = city;
        x.district  = district;
        x.barangay  = barangay;
        x.email     = email;
        x.firstName = firstName;
        x.lastName  = lastName;
        x.contact   = contact;
        x.address   = address;
        x.accesslevel = accesslevel;
        x.dateModifiedTime = DATE.dateWithTime();
        x.modifiedBy = modifiedby;
        x.save();
        console.log(x)
        res.send({ success: 'Successfully saved' })
    } catch (e) {
        res.send({ error: e.message })
    }
}

// router.delete('/d/admin', controllerAccount.delete_admin)
const delete_record = async (req, res) => {
    try {
        const region    = req.body.region;
        const province  = req.body.province;
        const city      = req.body.city;
        const district  = req.body.district;
        const barangay  = req.body.barangay;
        const email     = req.body.email;
        const modifiedby = req.body.modifiedBy;

        //const x = await User.findOne({username: username});
        // const x = await City.deleteOne({ region: region, province: province, code: code });
        console.log('Deleted reg=' + region + ' prov=' + province + ' city=' + city + ' dist=' + district + ' barangay=' + barangay + ' email=' + email)
        const x = await UserRegistration.findOne({ region: region, province: province, city: city, district: district, barangay: barangay, email: email, status: 1 });
        if (!x) throw createError(403, `Code not found!`);
        x.dateModifiedTime = DATE.dateWithTime();
        x.modifiedBy = modifiedby;
        x.status = 0;
        x.save();
        res.send({ success: 'Successfully deleted' })

    } catch (e) {
        res.send({ error: e.message })
    }
}

/*
//Delete item from database
router.delete('/api/item/:id', async (req, res)=>{
    try{
      //find the item by its id and delete it
      const deleteItem = await todoItemsModel.findByIdAndDelete(req.params.id);
      res.status(200).json('Item Deleted');
    }catch(err){
      res.json(err);
    }
  })
*/

    // create_record,
module.exports = {
    get_region,
    get_province,
    get_city,
    get_district,
    get_barangay,
    get_record,
    get_record_user,
    // create_record,
    update_record,
    delete_record,
};



const isDevStage = false;

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h*60*60*1000));
    return this;
}
Date.prototype.addTime = function(h){
    this.setTime(this.getTime() + (h*60*1000));
    return this;
}

Date.prototype.addSec = function(s){
    this.setTime(this.getTime() + (s*1000));
    return this;
}

const dateWithTime  = () => {
    return isDevStage ? new Date().addHours(8).toISOString() : new Date().addHours(8).toISOString();
} 

const dateOnly = () => {
    return isDevStage ? new Date().toISOString().slice(0,10) : new Date().addHours(8).toISOString().slice(0,10);
} 

const dateValid35 = () => {
    return isDevStage ? new Date().addSec(35).toISOString() : new Date().addHours(8).addSec(35).toISOString();
}

const dateValid60 = () => {
    return isDevStage ? new Date().addSec(60).toISOString() : new Date().addHours(8).addSec(60).toISOString();
}

const dateValid120 = () => {
    return isDevStage ? new Date().addSec(120).toISOString() : new Date().addHours(8).addSec(120).toISOString();
}

const dateLoginExpiration = () => {
    return isDevStage ? new Date().getTime() + 1000 * 60 * 60 : new Date().getTime() + 1000 * 60 * 60 * 9;
}

const cookieExpiration = () => {
    return new Date().getTime() +  60 * 60 * 10;
}


let date_ob = new Date();

// current date
// adjust 0 before single digit date
let date = ("0" + date_ob.getDate()).slice(-2);

// current month
let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

// current year
let year = date_ob.getFullYear();

// current hours
let hours = date_ob.getHours();

// current minutes
let minutes = date_ob.getMinutes();

// current seconds
let seconds = date_ob.getSeconds();

// prints date in YYYY-MM-DD format
console.log(year + "-" + month + "-" + date);

// prints date & time in YYYY-MM-DD HH:MM:SS format
console.log(year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds);

// prints time in HH:MM format
console.log(hours + ":" + minutes);

const timeOnly = () => {
    let date_ob = new Date();
    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    if (minutes < 10){ minutes = '0' + minutes}
    let seconds = date_ob.getSeconds();
    if (seconds < 10) { seconds = '0' + seconds }
    let currtime = hours + ':' + minutes + ':' + seconds;
    return currtime;
} 

module.exports = {
    dateOnly,
    timeOnly,
    dateWithTime,
    dateValid35,
    dateValid60,
    dateValid120,
    dateLoginExpiration,
    cookieExpiration
}
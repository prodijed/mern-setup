// import './App.css';
import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom"
import { UserContext } from "./UserContext";



import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//components
import Home from './pages/Home';
import Signup from './pages/Signup';
import Login from './pages/Login';
import Header from './components/Header';
import SideBar from './components/Sidebar/SideBar';
import Users_Registration from './pages/users_registration/users_Registration.jsx';

// functions
import { getUser } from "./api/user";
import { HomeMax } from '@mui/icons-material';

function App() {

  const containerStyle = {
    display: 'flex',
    flexDirection: 'row', // Make it row-based
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  };

  const [user, setUser] = useState(null);
  const [location, setLocation] = useState(null);

  useEffect(() => {
    const unsubscribe = getUser()
      .then((res) => {
        if (res.error) {

          toast(res.error);
        } else {
          alert(res.username);
          setUser(res.username);
        }
      })
      .catch((err) => toast(err));

    return () => unsubscribe;
  }, []);
  return (
    <div className="app-container">
      <Router>
        <UserContext.Provider value={{ user, setUser, location, setLocation }}>
          <ToastContainer />
          <Header />
          <div style={containerStyle}>
          <SideBar>
            <Routes>
              
                <Route path="/" element={<Login />} />
                <Route path="/home" element={<Home />} />
                <Route path="/signup" element={<Signup />} />
                <Route path="/users_registration" element={<Users_Registration />} />
             
            </Routes>
            </SideBar>
          </div>
        </UserContext.Provider>
      </Router>
    </div>
  );
}

export default App;

//import User from './pages/users_registration/users_Registration.jsx';

//import UserRegistration from './pages/users_registration/users_Registration';
{/* <Router>
      <UserContext.Provider value={{ user, setUser, location, setLocation }}>
        <ToastContainer />
        <Header />
        <div style={containerStyle}>
          <Routes>
            <Route path="/" element={<Login />} />
            
            <Route path="/home" element={<>
              <SideBar />
              <Home />
            </>} />
            <Route path="/signup" element={<Signup />} />
            
          </Routes>
        </div>
        </UserContext.Provider>
      </Router> */}
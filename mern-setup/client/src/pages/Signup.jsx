import React, { useState } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { toast } from "react-toastify";
import Select from "react-select";


// design
import {
  TextField,
  InputAdornment,
  IconButton,
  OutlinedInput,
  FormControl,
  InputLabel,
  Button,
  FormHelperText,
} from "@mui/material";

import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import VisibilityIcon from "@mui/icons-material/Visibility";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import CancelIcon from "@mui/icons-material/Cancel";

// functions
import { register } from "../api/user";

const Signup = () => {
  const navigate = useNavigate();
  // form states
  const [selectedOptionRegion, setSelectedOptionRegion] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  


  //password validation
  let hasSixChar = password.length >= 6;
  let hasLowerChar = /(.*[a-z].*)/.test(password);
  let hasUpperChar = /(.*[A-Z].*)/.test(password);
  let hasNumber = /(.*[0-9].*)/.test(password);
  let hasSpecialChar = /(.*[^a-zA-Z0-9].*)/.test(password);


  const apiData = () =>{
    
  }
  useEffect(() => {
       getApiData();
}, []);

  const handleRegister = async (e) => {
    e.preventDefault();

    try {
      const res = await register({ username, email, password });
      if (res.error) {
        // Use toast.error to display the error message
        toast.error(res.error);
      } else {
        // Use toast.success to display the success message
        toast.success(res.message);
        // navigate the user to login
        navigate("/", { replace: true });
      }
    } catch (err) {
      // Use toast.error to display any caught exceptions
      toast.error(err.message); // Assuming err has a message property
    }
  };


  const onchangeSetSelectedOptionRegion = e => {
    setSelectedOptionRegion(e.target.value)
  }

  return (
    <div className="container mt-5 mb-5 col-10 col-small-8 col-md-6 col-lg-5" style={{ border: "1.5px solid black", padding: "15px", width: "500px" }}>
      <div className="text-center mb-5 alert alert-primary">
        <label htmlFor="" className="h2">
          Signup
        </label>
      </div>
      <div className="form-group">
        <Select className='Select'
          value={selectedOptionRegion}
          onChange={onchangeSetSelectedOptionRegion}
          options={options}
          placeholder="Select Region"
          // styles={selectStyles}
        />
      </div>
      <div className="form-group">
        <TextField
          size="small"
          variant="outlined"
          className="form-control"
          label="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          style={{ width: "100%" }} // Set the width to 100%
        />
      </div>

      <div className="form-group">
        <TextField
          size="small"
          variant="outlined"
          className="form-control"
          label="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          style={{ width: "100%" }} // Set the width to 100%
        />
      </div>
      <div className="form-group">
        <FormControl
          variant="outlined"
          size="small"
          className="form-control"
          style={{ width: "100%" }}
        >
          <InputLabel>Password</InputLabel>
          <OutlinedInput
            label="Password"
            type={showPassword ? "text" : "password"}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  edge="end"
                  onClick={() => setShowPassword(!showPassword)}
                >
                  {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
        {password && (
          <div className="ml-1" style={{ columns: 2 }}>
            <div>
              {hasSixChar ? (
                <span className="text-success">
                  <CheckCircleIcon className="mr-1" fontSize="small" />
                  <small>at least 6 characters</small>
                </span>
              ) : (
                <span className="text-danger">
                  <CancelIcon className="mr-1" fontSize="small" />
                  <small>at least 6 characters</small>
                </span>
              )}
            </div>
            <div>
              {hasLowerChar ? (
                <span className="text-success">
                  <CheckCircleIcon className="mr-1" fontSize="small" />
                  <small>one lowercase</small>
                </span>
              ) : (
                <span className="text-danger">
                  <CancelIcon className="mr-1" fontSize="small" />
                  <small>one lowercase</small>
                </span>
              )}
            </div>
            <div>
              {hasUpperChar ? (
                <span className="text-success">
                  <CheckCircleIcon className="mr-1" fontSize="small" />
                  <small>one uppercase</small>
                </span>
              ) : (
                <span className="text-danger">
                  <CancelIcon className="mr-1" fontSize="small" />
                  <small>one uppercase</small>
                </span>
              )}
            </div>
            <div>
              {hasNumber ? (
                <span className="text-success">
                  <CheckCircleIcon className="mr-1" fontSize="small" />
                  <small>one number</small>
                </span>
              ) : (
                <span className="text-danger">
                  <CancelIcon className="mr-1" fontSize="small" />
                  <small>one number</small>
                </span>
              )}
            </div>
            <div>
              {hasSpecialChar ? (
                <span className="text-success">
                  <CheckCircleIcon className="mr-1" fontSize="small" />
                  <small>one special symbol</small>
                </span>
              ) : (
                <span className="text-danger">
                  <CancelIcon className="mr-1" fontSize="small" />
                  <small>one special symbol</small>
                </span>
              )}
            </div>
          </div>
        )}
      </div>
      <div className="form-group">
        <TextField
          size="small"
          type="password"
          variant="outlined"
          className="form-control"
          label="Confirm Password"
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}
        />
        {password && confirmPassword && (
          <FormHelperText className="ml-1 mt-1">
            {password === confirmPassword ? (
              <span className="text-success">Password does match</span>
            ) : (
              <span className="text-danger">Password does not match</span>
            )}
          </FormHelperText>
        )}
      </div>

      <div className="text-center mt-4">
        <Button
          variant="contained"
          disabled={
            !username ||
            !email ||
            !password ||
            !confirmPassword ||
            password !== confirmPassword ||
            !hasSixChar ||
            !hasLowerChar ||
            !hasUpperChar ||
            !hasNumber ||
            !hasSpecialChar
          }
          onClick={handleRegister}
        >
          Submit
        </Button>
      </div>
    </div>
  );
};

export default Signup;
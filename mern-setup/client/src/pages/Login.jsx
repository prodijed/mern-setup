import React, { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { UserContext } from "../UserContext";
// design
import {
  TextField,
  InputAdornment,
  IconButton,
  OutlinedInput,
  FormControl,
  InputLabel,
  Button,
} from '@mui/material';

import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import VisibilityIcon from '@mui/icons-material/Visibility';

// functions
import { login } from '../api/user';

const Login = () => {
  const navigate = useNavigate();
  const { user, setUser } = useContext(UserContext);
  const { location, setLocation } = useContext(UserContext);

  // form states
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);



  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const res = await login({ email, password });
      if (res.error) {
        toast.error(res.error);
      } else {
        alert("user = " + JSON.stringify(res));
        toast.success(res.message);
        setUser(res.username); // Assuming res.user contains user data
        setLocation({
          region: res.region,
          province: res.province,
          city: res.city,
          district: res.district,
          barangay: res.barangay,
        })
        // Redirect the user to home ("/")
        navigate('/home', { replace: true });
      }
    } catch (err) {
      toast.error(err.message);
    }
  };
  return (
    <div className="container mt-5 mb-5 col-10 col-small-8 col-md-6 col-lg-5" style={{ border: "1.5px solid black", padding: "15px", width: "500px" }}>
    <div className="text-center mb-5 alert alert-primary">
        <label htmlFor="" className="h2">
          Login
        </label>
      </div>

      <div className="form-group">
        <TextField
          size="small"
          variant="outlined"
          className="form-control"
          label="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          style={{ width: '100%' }} // Set the width to 100%
        />
      </div>
      <div className="form-group">
        <FormControl variant="outlined" size="small" className="form-control" style={{ width: '100%' }}>
          <InputLabel>Password</InputLabel>
          <OutlinedInput
            label="Password"
            type={showPassword ? 'text' : 'password'}
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            endAdornment={
              <InputAdornment>
                <IconButton edge="end" onClick={() => setShowPassword(!showPassword)}>
                  {showPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </div>

      <div className="text-center mt-4">
        <Button variant="contained" disabled={!email || !password}
        		onClick={handleLogin}
        >
          Submit
        </Button>

      </div>
    </div>
  );
};

export default Login;

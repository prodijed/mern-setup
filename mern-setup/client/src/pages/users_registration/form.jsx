import React, { useState, useEffect, useContext } from "react";
// import { useHistory, Redirect } from "react-router-dom"; //work on v5
import { useNavigate } from 'react-router-dom';  // replaced by this on v6
import Select from 'react-select';
import { Container, Row, Col } from 'react-grid-system';

import { Backdrop } from '@mui/material';
import { CircularProgress } from '@mui/material';

// design
import {
    TextField, InputAdornment, IconButton, OutlinedInput, FormControl,
    InputLabel, FormHelperText, Button as ButtonSubmit
} from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import CancelIcon from "@mui/icons-material/Cancel";
// import Button from "../../shared/components/FormElements/Button";
import { Button } from '@mui/material';
import { useFetch } from "../../../shared/components/Hooks/http_hook_users_Registration";

// functions
// import { useFetch } from '../../shared/components/Hook/http_hook_UsersRegistration';
// import { register } from "../api/user";
// import { register, useFetch } from "../../shared/components/Hook/http_hook_users_Registration";
//import { register, useFetch } from "../../shared/components/Hook/http_hook_Users_Registration";
// import { register } from "../../shared/components/Hook/http_hook_Users_registration";
// import { register, useFetch } from "../../shared/components/Hook/http_hook_users_Registration";

import { UserContext } from "../../UserContext";
import { grey } from '@mui/material/colors';
import { grid } from '@mui/system';
import { toast } from 'react-toastify';

let options = [];
let options_province = [];
let options_city = [];
let options_district = [];
let options_barangay = [];

let options_accesslevel = [
    { value: '1', label: '1 : View Only' },
    { value: '3', label: '3 : Reports Only' },
    { value: '5', label: '5 : User' },
    { value: '7', label: '7 : Barangay Officers' },
    { value: '9', label: '9 : Administrator' },
    { value: '11', label: '11 : District' },
    { value: '13', label: '13 : Cities/Municipalities' },
    { value: '15', label: '15 : Provincial' },
    { value: '17', label: '17 : Regional' },
    { value: '19', label: '19 : National' },
    { value: '21', label: '21 : Superuser' },
];


const form = (props) => {
    const { user } = useContext(UserContext);    
    // Loading Indicator
    const [loadingIndicator, setLoadingIndicator] = React.useState(false);

    // Clear inputs
    const [clearInput, setClearInput] = useState({ contact: '', address: '' });

    // Add, Refresh, Save, Cancel
    const [isAddRefresh, setIsAddRefresh] = useState(true);
    const [isSaveCancel, setIsSaveCancel] = useState(false);

    const { sendRequest } = useFetch();
    const [loggedMessage, setLoggeedMessage] = useState();
    const [isLoading, setIsLoading] = useState(false)
    const [data, setData] = useState();

    /*
    const [formState, setFormState] = useState({
        region: { value: null, name: null, isValid: false },
        province: { value: null, name: null, isValid: false },
        city: { value: null, name: null, isValid: false },
        district: { value: null, name: null, isValid: false },
        barangay: { value: null, name: null, isValid: false },
        contact: { value: null, name: null, isValid: false },
        description: { value: null, name: null, isValid: false },
    });
    */

    const [param, setParam] = useState({ action: '' });




    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [contact, setContact] = useState('');
    const [address, setAddress] = useState('');
    // Start Sign Up
    // form states
    const navigate = useNavigate();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [showPassword, setShowPassword] = useState(false);
    // password validation
    let hasSixChar = password.length >= 6;
    let hasLowerChar = /(.*[a-z].*)/.test(password);
    let hasUpperChar = /(.*[A-Z].*)/.test(password);
    let hasNumber = /(.*[0-9].*)/.test(password);
    let hasSpecialChar = /(.*[^a-zA-Z0-9].*)/.test(password);
    // End Sign Up






    const [commandAction, setCommandAction] = useState(true);
    const disableComponent = (status) => {
        setCommandAction(status)
    }

    const createHandler = async (e) => {
        // Check if specific Region and Province
        setLoadingIndicator(true);
        if (selectedOptionRegion.value == 'All' || selectedOptionProvince.value == 'All' || selectedOptionCity.value == 'All') {
            toast.warning('Please select specific region and province and city!', {
                position: "top-center", theme: "colored", autoClose: 3000,
                hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true,
                progress: '',
            });
            setLoadingIndicator(false);
            return;
        }

        clearHandleInput();

        // ref.current.value = '';
        const param = {
            action: "addRecord",
            region_code: selectedOptionRegion.value,
            region_label: selectedOptionRegion.label,
            province_code: selectedOptionProvince.value,
            province_label: selectedOptionProvince.label,
            city_code: selectedOptionCity.value,
            city_label: selectedOptionCity.label,
            district_code: selectedOptionDistrict.value,
            district_label: selectedOptionDistrict.label,
            barangay_code: selectedOptionBarangay.value,
            barangay_label: selectedOptionBarangay.label
        };
        // alert('add= ' + JSON.stringify(param))
        setParam(param);
        props.onCountChanged(param);
        setLoadingIndicator(false);
        setIsAddRefresh(false)
        setIsSaveCancel(true)
        disableComponent(false);
    }


    const refreshHandler = async (e) => {
        const param = {
            action: "refreshUser",
            region_code: selectedOptionRegion.value,
            region_label: selectedOptionRegion.label,
            province_code: selectedOptionProvince.value,
            province_label: selectedOptionProvince.label,
            city_code: selectedOptionCity.value,
            city_label: selectedOptionCity.label,
            district_code: selectedOptionDistrict.value,
            district_label: selectedOptionDistrict.label,
            barangay_code: selectedOptionBarangay.value,
            barangay_label: selectedOptionBarangay.label
        };
        // alert('click refresh=' + JSON.stringify(param))
        setParam(param);
        props.onCountChanged(param);
        disableComponent(true)
    }

    const [executeRegion, setExecuteRegion] = useState('');
    const [selectedOptionRegion, setSelectedOptionRegion] = useState(null);
    const [executeProvince, setExecuteProvince] = useState('')
    const [selectedOptionProvince, setSelectedOptionProvince] = useState(null);
    const [executeCity, setExecuteCity] = useState('');
    const [selectedOptionCity, setSelectedOptionCity] = useState(null);
    const [executeDistrict, setExecuteDistrict] = useState('');
    const [selectedOptionDistrict, setSelectedOptionDistrict] = useState(null);
    const [executeBarangay, setExecuteBarangay] = useState('');
    const [selectedOptionBarangay, setSelectedOptionBarangay] = useState(null);

    const [executeAccessLevel, setExecuteAccessLevel] = useState('');
    const [selectedOptionAccessLevel, setSelectedOptionAccessLevel] = useState(null);

    const onchangeSetSelectedOptionRegion = async (e) => {
        setExecuteRegion('Yes');
        await setSelectedOptionRegion({ value: e.value, label: e.label })
    }
    const onchangeSetSelectedOptionProvince = async (e) => {
        setExecuteProvince('Yes');
        await setSelectedOptionProvince({ value: e.value, label: e.label });
    }
    const onchangeSetSelectedOptionCity = async (e) => {
        setExecuteCity('Yes');
        await setSelectedOptionCity({ value: e.value, label: e.label });
    }
    const onchangeSetSelectedOptionDistrict = async (e) => {
        setExecuteDistrict('Yes');
        await setSelectedOptionDistrict({ value: e.value, label: e.label });
    }

    const onchangeSetSelectedOptionBarangay = async (e) => {
        setExecuteBarangay('Yes');
        await setSelectedOptionBarangay({ value: e.value, label: e.label });
    }

    const onchangeSetSelectedOptionAccessLevel = async (e) => {
        setExecuteAccessLevel('Yes');
        await setSelectedOptionAccessLevel({ value: e.value, label: e.label });
    }

    useEffect(() => {
        if (executeRegion == 'Yes') {
            setExecuteRegion('');
            getApiDataProvince();
            // onchangeRegionProvinceCity();
        }
    }, [selectedOptionRegion])
    useEffect(() => {
        if (executeProvince == 'Yes') {
            setExecuteProvince('');
            getApiDataCity();
            // onchangeRegionProvinceCity();
        }
    }, [selectedOptionProvince])
    useEffect(() => {
        if (executeCity == 'Yes') {
            setExecuteCity('');
            getApiDataDistrict();
            // onchangeRegionProvinceCityDistrict();
        }
    }, [selectedOptionCity])
    useEffect(() => {
        if (executeDistrict == 'Yes') {
            setExecuteDistrict('');
            onchangeRegionProvinceCityDistrictBarangay();
        }
    }, [selectedOptionDistrict])

    useEffect(() => {
        if (executeAccessLevel == 'Yes') {
            setExecuteAccessLevel('');
        }
    }, [selectedOptionAccessLevel])

    const onchangeRegionProvinceCityDistrictBarangay = () => {
        let regi = selectedOptionRegion;
        let prov = selectedOptionProvince;
        let city = selectedOptionCity;
        let dist = selectedOptionDistrict;

        const param = {
            action: "refreshBarangay",
            region_code: regi.value,
            region_label: regi.label,
            province_code: prov.value,
            province_label: prov.label,
            city_code: city.value,
            city_label: city.label,
            district_code: dist.value,
            district_label: dist.label
        };
        console.log('RegProvCityDistrict param = ' + JSON.stringify(param))
        // alert('RegProvCity param = ' + JSON.stringify(param))
        setParam(param);
        props.onCountChanged(param);
    }


    /*
    const saveHandler = async () => {
        try {
            alert('Click save')
            const details = {
                region: selectedOptionRegion.value,
                province: selectedOptionProvince.value,
                city: selectedOptionCity.value,
                district: selectedOptionDistrict.value,
                barangay: selectedOptionBarangay.value,
                firstName: firstName,
                lastName: lastName,
                contact: contact.trim(),
                address: address,
                username: username,
                password: password,
                confirm: confirm
            }


            if (selectedOptionRegion.value == 'All' || selectedOptionProvince.value == 'All' || selectedOptionCity.value == 'All' || selectedOptionDistrict.value == 'All' || selectedOptionBarangay.value == 'All') {
                toast.warning('Please select specific Region, Province, City, District, and Barangay!', {
                    position: "top-center", theme: "colored", autoClose: 3000,
                    hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true,
                    progress: '',
                });
                return;
            }

            if (details.firstName == '' || details.firstName == null) {
                toast.warning('Please enter firstname!', {
                    position: "top-center", theme: "colored", autoClose: 3000, hideProgressBar: true,
                    closeOnClick: true, pauseOnHover: true, draggable: true, progress: '',
                });
                return;
            }
            if (details.lastName == '' || details.lastName == null) {
                toast.warning('Please enter lastname!', {
                    position: "top-center", theme: "colored", autoClose: 3000, hideProgressBar: true,
                    closeOnClick: true, pauseOnHover: true, draggable: true, progress: '',
                });
                return;
            }
            if (details.username == '' || details.username == null) {
                toast.warning('Please enter username!', {
                    position: "top-center", theme: "colored", autoClose: 3000, hideProgressBar: true,
                    closeOnClick: true, pauseOnHover: true, draggable: true, progress: '',
                });
                return;
            }

            if (details.password == '' || details.password == null) {
                toast.warning('Please enter password!', {
                    position: "top-center", theme: "colored", autoClose: 3000, hideProgressBar: true,
                    closeOnClick: true, pauseOnHover: true, draggable: true, progress: '',
                });
                return;
            }
            if (details.confirm == '' || details.confirm == null) {
                toast.warning('Please enter confirm password!', {
                    position: "top-center", theme: "colored", autoClose: 3000, hideProgressBar: true,
                    closeOnClick: true, pauseOnHover: true, draggable: true, progress: '',
                });
                return;
            }

            if (details.password != details.confirm) {
                toast.warning('Password and confirm password do not match!', {
                    position: "top-center", theme: "colored", autoClose: 3000, hideProgressBar: true,
                    closeOnClick: true, pauseOnHover: true, draggable: true, progress: '',
                });
                return;
            }

            setIsLoading(true);
            // return setLoggeedMessage({ error: 'Please enter Sequence number value!' })

            alert('Clicked save and ready to save ' + JSON.stringify(details));
            const result = await sendRequest('/create/record', 'POST', details);
            setIsLoading(false);
            if (result.actionFB == 'Duplicate') {
                toast.error('Duplicate record! ' + result.error, {
                    position: "top-center", theme: "colored", autoClose: 3000,
                    hideProgressBar: true, closeOnClick: true,
                    pauseOnHover: true, draggable: true, progress: '',
                });
                return;
            }
            setIsAddRefresh(true)
            setIsSaveCancel(false)
            if (result.actionFB == 'Created') {
                // refreshAddTable();
                //LLT const res = await sendRequest('/g/record', 'GET');
                //LLT setData(res);

                const param = {
                    action: "saveRecord",
                    region_code: selectedOptionRegion.value,
                    region_label: selectedOptionRegion.label,
                    province_code: selectedOptionProvince.value,
                    province_label: selectedOptionProvince.label,
                    city_code: selectedOptionCity.value,
                    city_label: selectedOptionCity.label,
                    district_code: selectedOptionDistrict.value,
                    district_label: selectedOptionDistrict.label,
                    barangay_code: selectedOptionBarangay.value,
                    barangay_label: selectedOptionBarangay.label,
                };
                setParam(param);
                props.onCountChanged(param);
                toast.success('Successfully Save! ', {
                    position: "top-center", theme: "colored", autoClose: 3000,
                    hideProgressBar: true, closeOnClick: true,
                    pauseOnHover: true, draggable: true, progress: '',
                });
            }

            if (result && result.error) return setLoggeedMessage({ error: result.error })
        } catch (e) {
            setIsLoading(false);
            console.log(e)
            setLoggeedMessage({ error: e.message })
            toast.error('Error : ' + e.message, {
                position: "top-center", theme: "colored", autoClose: 3000,
                hideProgressBar: true, closeOnClick: true,
                pauseOnHover: true, draggable: true, progress: '',
            });
        }
    }
    */


    const cancelHandler = async () => {
        const param = { action: 'cancelRecord' };
        setParam(param);
        props.onCountChanged(param);
        setIsAddRefresh(true)
        setIsSaveCancel(false)
        disableComponent(true)
        setPassword('');
        setConfirmPassword('');
    }    
    






    // onload Refresh
    let loadCounterOnce = 0;
    const getApiData = async () => {
        setLoadingIndicator(true);
        // Region, "http://localhost:3001/user_registration/l/region/"
        const response = await fetch(
            `${import.meta.env.VITE_REACT_APP_API_URL}/user_registration/l/region/`
        ).then((response) => response.json());
        options = []
        for (let i = 0; i < response.length; i++) {
            options.push({ value: response[i].code, label: response[i].description })
        }
        options.push({ value: 'All', label: 'All Regions' })
        setSelectedOptionRegion(options[0]);


        // Province
        let region_code = options[0].value;
        // "http://localhost:3001/user_registration/l/province?region_code=" + region_code
        const response_province = await fetch(
            `${import.meta.env.VITE_REACT_APP_API_URL}/user_registration/l/province?region_code=` + region_code
        ).then((response_province) => response_province.json());
        options_province = []
        for (let i = 0; i < response_province.length; i++) {
            options_province.push({ value: response_province[i].code, label: response_province[i].description })
        }
        setSelectedOptionProvince([]);
        options_province.push({ value: 'All', label: 'All Province' })
        setSelectedOptionProvince(options_province[0]);


        // City
        let province_code = options_province[0].value;
        const response_city = await fetch(
            //                  "http://localhost:3001/user_registration/l/city?region_code=" + region_code + "&province_code=" + province_code
            `${import.meta.env.VITE_REACT_APP_API_URL}/user_registration/l/city?region_code=` + region_code + '&province_code=' + province_code
        ).then((response_city) => response_city.json());
        options_city = []
        for (let i = 0; i < response_city.length; i++) {
            options_city.push({ value: response_city[i].code, label: response_city[i].description })
        }
        setSelectedOptionCity([]);
        options_city.push({ value: 'All', label: 'All Cities' })
        setSelectedOptionCity(options_city[0]);

        // District
        let city_code = options_city[0].value;
        const response_district = await fetch(
            //                  "http://localhost:3001/user_registration/l/district?region_code=" + region_code + "&province_code=" + province_code + "&city_code=" + city_code
            `${import.meta.env.VITE_REACT_APP_API_URL}/user_registration/l/district?region_code=` + region_code + '&province_code=' + province_code + '&city_code=' + city_code
        ).then((response_district) => response_district.json());
        options_district = []
        for (let i = 0; i < response_district.length; i++) {
            options_district.push({ value: response_district[i].code, label: response_district[i].description })
        }
        setSelectedOptionDistrict([]);
        options_district.push({ value: 'All', label: 'All Districts' })
        setSelectedOptionDistrict(options_district[0]);


        // Barangay
        let district_code = options_district[0].value;
        const response_barangay = await fetch(
            //                  "http://localhost:3001/user_registration/l/barangay?region_code=" + region_code + "&province_code=" + province_code + "&city_code=" + city_code + "&district_code=" + district_code
            `${import.meta.env.VITE_REACT_APP_API_URL}/user_registration/l/barangay?region_code=` + region_code + '&province_code=' + province_code + '&city_code=' + city_code + '&district_code=' + district_code
        ).then((response_barangay) => response_barangay.json());
        options_barangay = []
        for (let i = 0; i < response_barangay.length; i++) {
            options_barangay.push({ value: response_barangay[i].code, label: response_barangay[i].description })
        }
        setSelectedOptionBarangay([]);
        options_barangay.push({ value: 'All', label: 'All Barangays' })
        setSelectedOptionBarangay(options_barangay[0]);

        const param = {
            action: "refreshUser", region_code: options[0].value, region_label: options[0].label,
            province_code: options_province[0].value, province_label: options_province[0].label,
            city_code: options_city[0].value, city_label: options_city[0].label,
            district_code: options_district[0].value, district_label: options_district[0].label,
            barangay_code: options_barangay[0].value, barangay_label: options_barangay[0].label
        };

        setParam(param);
        props.onCountChanged(param);
        setLoadingIndicator(false);
    }



  // onchange in region
  const getApiDataProvince = async () => {
    // alert('getApiDataProvince=' + JSON.stringify(selectedOptionRegion));
    let region_code = selectedOptionRegion.value;
    const response_province = await fetch(
      "http://localhost:3001/barangay/l/province?region_code=" + region_code
    ).then((response_province) => response_province.json());
    options_province = []
    for (let i = 0; i < response_province.length; i++) {
      options_province.push({ value: response_province[i].code, label: response_province[i].description })
    }
    setSelectedOptionProvince([]);
    options_province.push({ value: 'All', label: 'All Provinces' })
    setSelectedOptionProvince(options_province[0]);
    setExecuteProvince('Yes');
  }

  // onchange in province
  const getApiDataCity = async () => {
    // alert('getApiDataCity=' + JSON.stringify(selectedOptionCity));
    let region_code = selectedOptionRegion.value;
    let province_code = selectedOptionProvince.value;
    const response_city = await fetch(
      "http://localhost:3001/barangay/l/city?region_code=" + region_code + "&province_code=" + province_code
    ).then((response_city) => response_city.json());
    options_city = []
    for (let i = 0; i < response_city.length; i++) {
      options_city.push({ value: response_city[i].code, label: response_city[i].description })
    }
    setSelectedOptionCity([]);
    options_city.push({ value: 'All', label: 'All Cities' })
    setSelectedOptionCity(options_city[0]);
    setExecuteCity('Yes');
  }

  // onchange in region, province, city
  const getApiDataDistrict = async () => {
    // alert('getApiDataCity=' + JSON.stringify(selectedOptionCity));
    let region_code = selectedOptionRegion.value;
    let province_code = selectedOptionProvince.value;
    let city_code = selectedOptionCity.value;
    const response_district = await fetch(
      "http://localhost:3001/barangay/l/district?region_code=" + region_code + "&province_code=" + province_code + "&city_code=" + city_code
    ).then((response_district) => response_district.json());
    options_district = []
    for (let i = 0; i < response_district.length; i++) {
      options_district.push({ value: response_district[i].code, label: response_district[i].description })
    }
    setSelectedOptionDistrict([]);
    options_district.push({ value: 'All', label: 'All Districts' })
    setSelectedOptionDistrict(options_district[0]);
    setExecuteDistrict('Yes');
  }

    

    useEffect(() => {
        if (loadCounterOnce == 0) {
            getApiData();
        }
        loadCounterOnce++;
    }, []);





    const clearHandleInput = () => {
        setFirstName(''); setLastName('');
        //setContact('');
        //setAddress(''); setUsername(''); setPassword(''); setConfirm('');
    };





    const submitHandlerRegister = async (e) => {
        e.preventDefault();
        try {

            const input_details = {
                region: selectedOptionRegion.value,
                province: selectedOptionProvince.value,
                city: selectedOptionCity.value,
                district: selectedOptionDistrict.value,
                barangay: selectedOptionBarangay.value,
                firstName: firstName,
                lastName: lastName,
                contact: contact,
                address: address,
                accesslevel: selectedOptionAccessLevel.value,
                email: email,
                password: password,
                createdBy: user,
                modifiedBy: user
            }

            if (selectedOptionRegion.value == 'All' || selectedOptionProvince.value == 'All' || selectedOptionCity.value == 'All' || selectedOptionDistrict.value == 'All' || selectedOptionBarangay.value == 'All') {
                toast.error('Please select specific Region, Province, City, District, and Barangay!', {
                    position: "top-right", theme: "colored", autoClose: 3000,
                    hideProgressBar: true, closeOnClick: true, pauseOnHover: true, draggable: true,
                    progress: '',
                });
                return;
            }            

            // const res = await register({ email, password });
            const res = await register(input_details);
            alert('register=' + JSON.stringify(res))
            if (res.error) {
                toast.error(res.error, {
                    position: "top-right", theme: "colored", autoClose: 1000,
                    hideProgressBar: false, closeOnClick: false, pauseOnHover: true,
                    draggable: true, progress: '',
                });
            } else {
                // toast.success(res.message);
                // redirect the user to login
                toast.success(res.message, {
                    position: "top-right", theme: "colored", autoClose: 1500,
                    hideProgressBar: false, closeOnClick: false, pauseOnHover: true,
                    draggable: true, progress: '',
                });


                const param = {
                    action: "saveRecord",
                    region_code: selectedOptionRegion.value,
                    region_label: selectedOptionRegion.label,
                    province_code: selectedOptionProvince.value,
                    province_label: selectedOptionProvince.label,
                    city_code: selectedOptionCity.value,
                    city_label: selectedOptionCity.label,
                    district_code: selectedOptionDistrict.value,
                    district_label: selectedOptionDistrict.label,
                    barangay_code: selectedOptionBarangay.value,
                    barangay_label: selectedOptionBarangay.label,
                };
                setParam(param);
                props.onCountChanged(param);
                // navigate('/login', { replace: true })
            }
        } catch (error) {
            toast.error(error);
        }
    };

    const selectStyles = { menu: styles => ({ ...styles, zIndex: 999 }) };
    return (
        <>
            <Backdrop
                sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                open={loadingIndicator}
            >
                <CircularProgress color="inherit" />
            </Backdrop>


            <Container fluid >
                <Row debug style={{ marginTop: "15px", marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <div
                            style={{
                                display: "flex",
                                alignItems: "center",
                                justifyContent: "left",
                                gap: "10px",
                            }}
                        >

                            {isAddRefresh && <Button
                                padding="10px;"
                                margin="20px"
                                variant='primary'
                                className='button green'
                                title='Add'
                                onClick={createHandler}
                            >
                                Add
                            </Button>}
       
                            {isAddRefresh && <Button
                                variant='info'
                                className=''
                                title='Refresh'
                            >
                                Refresh
                            </Button>
                            }
                            {/*
                            {isSaveCancel && <Button
                                variant='success'
                                className='button green'
                                title='Save'
                                onClick={saveHandler}
                            >
                                Save
                            </Button>
                            }
                            */ }
                            {isSaveCancel && <Button
                                variant='danger'
                                className='red'
                                title='Cancel'
                                onClick={cancelHandler}
                            >
                                Cancel
                            </Button>
                            }
                        </div>
                    </Col>
                </Row>

                {loggedMessage && loggedMessage.error}
                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <Select className='Select'
                            value={selectedOptionRegion}
                            onChange={onchangeSetSelectedOptionRegion}
                            options={options}
                            placeholder="Select Region"
                            styles={selectStyles}
                        />
                    </Col>
                </Row>
                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <Select className='Select Province'
                            value={selectedOptionProvince}
                            onChange={onchangeSetSelectedOptionProvince}
                            options={options_province}
                            placeholder="Select Province"
                            styles={selectStyles}
                        />
                    </Col>
                </Row>

                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12} >
                        <Select className='Select City'
                            value={selectedOptionCity}
                            onChange={onchangeSetSelectedOptionCity}
                            options={options_city}
                            placeholder="Select City"
                            styles={selectStyles}
                        />
                    </Col>
                </Row>

                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <Select className='Select District'
                            value={selectedOptionDistrict}
                            onChange={onchangeSetSelectedOptionDistrict}
                            options={options_district}
                            placeholder="Select District"
                            styles={selectStyles}
                        />
                    </Col>
                </Row>

                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <Select className='Select Barangay'
                            value={selectedOptionBarangay}
                            onChange={onchangeSetSelectedOptionBarangay}
                            options={options_barangay}
                            placeholder="Select Barangay"
                            styles={selectStyles}
                        />
                    </Col>
                </Row>
          
                <Row debug style={{ marginBottom: "15px" }}>
                    <Col xs={6} md={6} >
                        <TextField type='text' className='form-control' label='First Name' size="small"
                            id='firstName' name='firstName' value={firstName} variant='outlined' placeholder='First Name'
                            onChange={(e) => setFirstName(e.target.value)}
                            autoComplete="off" disabled={commandAction ? "disabled" : ""}
                            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
                        />

                    </Col>
                    <Col xs={6} md={6} >
                        <TextField type='text' className='form-control' label='Last Name' size="small"
                            id='lastName' name='lastName' value={lastName} variant='outlined' placeholder='Last Name'
                            onChange={(e) => setLastName(e.target.value)}
                            autoComplete="off" disabled={commandAction ? "disabled" : ""}
                            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
                        />
                    </Col>
                </Row>
        
                
                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12} >
                        <TextField type='text' className='form-control' label='Contact Number' size="small"
                            id="contact" name='contact' value={contact} placeholder='Contact Number'
                            onChange={(e) => setContact(e.target.value)}
                            autoComplete="off" disabled={commandAction ? "disabled" : ""}
                            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
                        />
                    </Col>
                </Row>

                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12} >
                        <TextField type='text' className='form-control' label='Address' size="small"
                            id="address" name='address' placeholder='Address'
                            value={address}
                            onChange={(e) => setAddress(e.target.value)}
                            autoComplete="off" disabled={commandAction ? "disabled" : ""}
                            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
                        />
                    </Col>
                </Row>

                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <Select className='Select'

                            value={selectedOptionAccessLevel}
                            onChange={onchangeSetSelectedOptionAccessLevel}
                            options={options_accesslevel}
                            placeholder="Access Level"
                            styles={selectStyles}
                            disabled={commandAction ? "disabled" : ""}
                            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
                        />
                    </Col>
                </Row>
                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <TextField
                            size="small"
                            variant="outlined"
                            className="form-control"
                            label="Username (Email Address)"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            disabled={commandAction ? "disabled" : ""}
                            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
                        />
                    </Col>
                </Row>
                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <FormControl
                            variant="outlined"
                            size="small"
                            className="form-control"
                            disabled={commandAction ? "disabled" : ""}
                            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
                        >
                            <InputLabel>Password</InputLabel>
                            <OutlinedInput
                                label="Password"
                                type={showPassword ? "text" : "password"}
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                                endAdornment={
                                    <InputAdornment>
                                        <IconButton edge="end" onClick={() => setShowPassword(!showPassword)}>
                                            {showPassword ? (
                                                <VisibilityIcon />
                                            ) : (
                                                <VisibilityOffIcon />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                        {password && (
                            <div className="ml-1" style={{ columns: 2 }}>
                                <div>
                                    {hasSixChar ? (
                                        <span className="text-success">
                                            <CheckCircleIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>at least 6 characters</small>
                                        </span>
                                    ) : (
                                        <span className="text-danger">
                                            <CancelIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>at least 6 characters</small>
                                        </span>
                                    )}
                                </div>

                                <div>
                                    {hasLowerChar ? (
                                        <span className="text-success">
                                            <CheckCircleIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>has lowercase letter</small>
                                        </span>
                                    ) : (
                                        <span className="text-danger">
                                            <CancelIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>has lowercase letter</small>
                                        </span>
                                    )}
                                </div>

                                <div>
                                    {hasUpperChar ? (
                                        <span className="text-success">
                                            <CheckCircleIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>has uppercase letter</small>
                                        </span>
                                    ) : (
                                        <span className="text-danger">
                                            <CancelIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>has uppercase letter</small>
                                        </span>
                                    )}
                                </div>

                                <div>
                                    {hasNumber ? (
                                        <span className="text-success">
                                            <CheckCircleIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>has number</small>
                                        </span>
                                    ) : (
                                        <span className="text-danger">
                                            <CancelIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>has number</small>
                                        </span>
                                    )}
                                </div>

                                <div>
                                    {hasSpecialChar ? (
                                        <span className="text-success">
                                            <CheckCircleIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>has special symbol</small>
                                        </span>
                                    ) : (
                                        <span className="text-danger">
                                            <CancelIcon
                                                className="mr-1"
                                                fontSize="small"
                                            />
                                            <small>has special symbol</small>
                                        </span>
                                    )}
                                </div>
                            </div>
                        )}
                    </Col>
                </Row>
                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <TextField
                            size="small"
                            type="password"
                            variant="outlined"
                            className="form-control"
                            label="Confirm Password"
                            value={confirmPassword}
                            onChange={(e) => setConfirmPassword(e.target.value)}
                            disabled={commandAction ? "disabled" : ""}
                            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
                        />
                        {password && confirmPassword && (
                            <FormHelperText className="ml-1 mt-1">
                                {password === confirmPassword ? (
                                    <span className="text-success">
                                        Password does match
                                    </span>
                                ) : (
                                    <span className="text-danger">
                                        Password does not match
                                    </span>
                                )}
                            </FormHelperText>
                        )}
                    </Col>
                </Row>

                <Row debug style={{ marginBottom: "15px" }}>
                    <Col md={12} lg={12}>
                        <ButtonSubmit variant="contained" disabled={
                            !firstName ||
                            !lastName ||
                            !password ||
                            !confirmPassword ||
                            password !== confirmPassword ||
                            !hasSixChar ||
                            !hasLowerChar ||
                            !hasUpperChar ||
                            !hasNumber ||
                            !hasSpecialChar
                        }
                            // variant='primary'
                            // className='button green'
                            onClick={submitHandlerRegister}
                        >
                            Submit
                        </ButtonSubmit>
                    </Col>
                </Row>
            </Container>

        </>
    );
};

export default form;

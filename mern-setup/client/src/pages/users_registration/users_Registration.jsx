import React, { useState } from 'react';
import { Container, Row, Col } from 'react-grid-system';

import Table from './table';
import Form from './form';

import './users_Registration.css'

const Users_Registration = () => {
    const [param, setParam] = useState(0)

  // Loading Indicator
  const [open, setOpen] = React.useState(false);

    let receiver = (param) => {
        // no-op
    };

    const trigger = (param) => {
        receiver && receiver(param);
    }

    const receiverCreator = (handler) => {
        receiver = handler;
        setOpen(true);
        //alert('handler=' + handler);
    }

    //<Col className='table' xs={12} sm={12} md={8} lg={8}><Table/></Col>
    //<Col className='form' xs={12} sm={12} md={4} lg={4}><Form /></Col>       
    return (
        <div className="Users Registration">
            <Container fluid>
                <Row className='parent'>
                    <Col className='table' xs={12} sm={12} md={8} lg={8}><Table receiverCreator={receiverCreator} /></Col>
                    <Col className='form' xs={12} sm={12} md={4} lg={4}><Form onCountChanged={trigger} /></Col>
                </Row>
            </Container>
        </div>
    )
};

export default Users_Registration;

import React, { useCallback, useState, useEffect, useMemo, useContext } from 'react'
// import Select from 'react-select';

import { Backdrop } from '@mui/material';
import { CircularProgress } from '@mui/material';

import MaterialReactTable from 'material-react-table';

import { Box, Button, TextField, IconButton, Tooltip } from '@mui/material';
import { Delete, Edit } from '@mui/icons-material';

import { ToastContainer, toast } from 'react-toastify';
import 'material-react-toastify/dist/ReactToastify.css';

// import { useFetch } from '../../shared/components/Hook/http_hook_users_Registration';
import { UserContext } from "../../UserContext";
import zIndex from '@mui/material/styles/zIndex';
import { blue } from '@mui/material/colors';
import { useFetch } from "../../../shared/components/Hooks/http_hook_users_Registration";

/*
const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];
*/

const table = (props) => {
    const { user } = useContext(UserContext);
    const { reload } = props;

    // React-toastify info,success,warning,error,default
    // top-left, top-right, top-center, bottom-left, bottom-right,bottom-center
    const notify = () => toast('Wow so easy!');
    /*
    toast.error('🦄 Wow so easy!', {
        position: "bottom-left",
        theme: "light",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: ,
    });
    */

    // hooks
    const { sendRequest } = useFetch();
    // const tableHook = useTable();

    // Loading Indicator
    const [loadingIndicator, setLoadingIndicator] = React.useState(false);

    // ulitities
    const [loggedMessage, setLoggeedMessage] = useState();
    const [isLoading, setLoading] = useState(false)

    // DATA
    const [data, setData] = useState();
    const [columns, setColumns] = useState([]);
    const [rows, setRows] = useState([]);
    const [currentRegion, setCurrentRegion] = useState();
    const [currentProvince, setCurrentProvince ] = useState();
    const [currentCity, setCurrentCity ] = useState();  
    const [currentDistrict, setCurrentDistrict ] = useState();
    const [currentBarangay, setCurrentBarangay] = useState();

    const [regionOld, setRegionOld] = useState('');
    const [provinceOld, setProvinceOld] = useState('');
    const [cityOld, setCityOld] = useState('');   
    const [districtOld, setDistrictOld] = useState('');
    const [barangayOld, setBarangayOld] = useState('');
    const [emailOld, setEmailOld] = useState(''); 


    const [param, setParam] = useState({action:'',region_code:'', region_label:'', province_code:'', province_label:'', city_code:'', city_label:'', district_code:'', district_label:'', barangay_code:'', barangay_label:''});
    const onCountReceived = (param) => {
        //console.log('received para= ' + param.action + ' regioncode=' + param.region_code + ' ' + param.region_label +' provincecode=' + param.province_code + ' ' + param.province_label );
        setParam(param);
        // alert('received======' + JSON.stringify(param));

        setCurrentRegion(param.region_code);
        setCurrentProvince(param.province_code);
        setCurrentCity(param.city_code);
        setCurrentDistrict(param.district_code);
        setCurrentBarangay(param.barangay_code);
        if (param.action == 'refreshRecord') {
            // getHandler(); No longer use this
            // getHandler_refreshBarangay(param.region_code, param.province_code, param.city_code, param.district_code);
            alert('Not used...')
        } else if (param.action == 'refreshUser') {
            // alert('r=' + param.region_code + ' p=' + param.province_code + ' c=' + param.city_code, ' d=' + param.district_code)
            getHandler_refreshUser(param.region_code, param.province_code, param.city_code, param.district_code, param.barangay_code);            
        } else if (param.action == 'addRecord') {
            title = 'Add Record'
            setState(false)
        } else if (param.action == 'saveRecord') {
            title = 'Save Record'
            setState(true);
            getHandler_refreshUser(param.region_code, param.province_code, param.city_code, param.district_code, param.barangay_code);                      
        } else if (param.action == 'cancelRecord') {
            title = 'Cancel Record'
            setState(true);
        }
    }
    props.receiverCreator(onCountReceived);

    const [state, setState] = useState(true);
    /*
    useEffect(() => {
        title = 'Why';
        setState(state)
        console.log('why')
    })
    */



    const getHandler = async () => {
        console.log('lex getHandler')
        setLoadingIndicator(true);
        //toast.info('Refresh!', {
        //    position: "top-center",     theme: "dark",      autoClose: 3000,
        //    hideProgressBar: true,      closeOnClick: true, pauseOnHover: true,
        //    draggable: true,            progress: '',
        //});
        try {
            setLoading(true);
            const result = await sendRequest('/g/record', 'GET');
            setLoading(false);
            if (result && result.error) return setLoggeedMessage({ error: result.error });
            setData(result)
            setLoadingIndicator(false);
        } catch (e) {
            setLoading(false);
            console.log(e)
            setLoggeedMessage({ error: e.message })
        }
    }


    const getHandler_refreshUser = async (region_code, province_code, city_code, district_code, barangay_code) => {
        // alert('region_code=' + region_code + ' province_code=' + province_code + ' city_code=' + city_code + ' district_code=' + district_code);   
        setLoadingIndicator(true);
        /*
        toast.info('Refresh!', {
            position: "top-center",     theme: "colored",      autoClose: 500,
            hideProgressBar: true,      closeOnClick: true, pauseOnHover: true,
            draggable: true,            progress: '',
        });
        */
        try {
            setLoading(true);
            // alert('sendRequest');
            const result = await sendRequest('/g/record_user?region_code=' + region_code + '&province_code=' + province_code + '&city_code=' + city_code + '&district_code=' + district_code + '&barangay_code=' + barangay_code , 'GET');
            setLoading(false);
            console.log('up result=' + JSON.stringify(result))
            if (result && result.error) return setLoggeedMessage({ error: result.error });
            // console.log('down result' + JSON.stringify(result))
            setData(result)
            setLoadingIndicator(false);
        } catch (e) {
            setLoading(false);
            console.log(e)
            setLoggeedMessage({ error: e.message })
        }
    };




    useEffect(() => {
        setState(state);
        setLoadingIndicator(false)
        // getHandler();
    }, [])

    const tableHandler = () => {
        try {
            const column = [
                { header: 'Region', accessorKey: 'region', enableEditing: false, minSize:30, maxSize: 35 },
                { header: 'Province', accessorKey: 'province', enableEditing: false, size:25 },
                { header: 'City.Code', accessorKey: 'city', enableEditing: false, size:30 },                
                { header: 'City', accessorKey: 'city_name', enableEditing: false, minSize:60, maxSize:70 },
                { header: 'District.Code', accessorKey: 'district', enableEditing: false, size:30 },                
                { header: 'District', accessorKey: 'district_name', enableEditing: false, minSize:55, maxSize:60 },                
                { header: 'Barangay_Code', accessorKey: 'barangay', enableEditing: false, minSize:30, maxSize:35 }, 
                { header: 'Barangay', accessorKey: 'barangay_name', enableEditing: false, minSize:30, maxSize:35 },                                
                { header: 'User Name', accessorKey: 'email', minSize:40, maxSize:45 },
                { header: 'First Name', accessorKey: 'firstName' },
                { header: 'Last Name', accessorKey: 'lastName' },   
                { header: 'Contact', accessorKey: 'contact' },
                { header: 'Address', accessorKey: 'address' },                 
                { header: 'Date Modified', accessorKey: 'dateModifiedTime' },
            ];
            let row = [];

            // alert('data========' + JSON.stringify(data));
            data.forEach(x => {
                row.push({
                    region: x.region,
                    province: x.province,
                    city: x.city,
                    city_name: x.city_name,
                    district: x.district,
                    district_name: x.district_name,
                    barangay: x.barangay,
                    barangay_name: x.barangay_name,                    
                    email: x.email,
                    firstName: x.firstName,
                    lastName: x.lastName,
                    contact: x.contact,
                    address: x.address,
                    dateModifiedTime: x.dateModifiedTime,
                    id: x._id
                })
            })
            setColumns(column)
            setRows(row)

        } catch (e) {
            console.log(e)
        }
    }


    useEffect(() => {
        // if (!data || data.length === 0) return;
        tableHandler();
    }, [data])




    // const editHandler = async(val) => {

    const editHandler = async ({ exitEditingMode, row, values }) => {
        data[row.index] = values;
        setLoading(true); 
        try {
            console.log('Editing details')

            const details = {
                region: values.region,
                province: values.province,
                city: values.city,
                district: values.district,
                barangay: values.barangay,
                email: values.email,
                firstName: values.firstName,
                lastName: values.lastName,
                contact: values.contact,
                address: values.address,
                accesslevel: values.accesslevel,
                modifiedBy: user
            }
            let region_code = details.region;
            let province_code = details.province;
            let city_code = details.city;
            let district_code = details.district;
            let barangay_code = details.barangay;


            console.log(JSON.stringify(details))
            let region_old = regionOld;
            let province_old = provinceOld;
            let city_old = cityOld;
            let district_old = districtOld;
            let barangay_old = barangayOld;
            let email_old = emailOld;
            // alert('updating reg_old=' + regionOld + ' prov_old=' + province_old + ' city_old=' + city_old +  ' dist_old=' + district_old + ' brgy_old=' + barangay_old + ' username_old=' + username_old)
            const result = await sendRequest('/u/record?region_old=' + region_old + '&province_old=' + province_old + '&city_old=' + city_old + '&district_old=' + district_old + '&barangay_old=' + barangay_old + '&email_old=' + email_old , 'POST', details);
            alert('result=' + JSON.stringify(result))
            console.log('LLT setLoading')
            if (result && result.error) return setLoggeedMessage({ error: result.error });

            // Filtering should be based in combo form, param not work here
            //region_code = param.region_code;
            //province_code = param.province_code;
            //city_code = param.city_code;
            //district_code = param.district_code;
            //barangay_code = param.barangay_code;
            getHandler_refreshUser(region_code, province_code, city_code, district_code, barangay_code)
            toast.info('Successfully updated!', {
                position: "top-right",     theme: "colored",      autoClose: 500,
                hideProgressBar: true,      closeOnClick: true, pauseOnHover: true,
                draggable: true,            progress: '',
            });
        } catch (e) {
            setLoggeedMessage({ error: e.message })
        }
        exitEditingMode();
        setLoading(false);
        // setLoading(true); //required to exit editing mode
    }


    const handleDeleteRow = async (row) => {
        //const values = val.values
        // alert('rows=' + row);
        try {
            console.log('Delete details')
            setLoading(true);
            const details = {
                region: row.getValue('region'),
                province: row.getValue('province'),    
                city: row.getValue('city'),
                district: row.getValue('district'),     
                barangay: row.getValue('barangay'),     
                email: row.getValue('email'),
                firstName: row.getValue('firstName'),
                modifiedBy: user
            }

            let region_code = details.region;
            let province_code = details.province;
            let city_code = details.city;
            let district_code = details.district;
            let barangay_code = details.barangay;
            let email       = details.email;
            if (
                !window.confirm(`Are you sure you want to delete ${row.getValue('email')}`)
            ) {
                // console.log('Return delete..')
                setLoadingIndicator(false);
                return;
            }
            console.log('Pasok' + JSON.stringify(details));
            const result = await sendRequest('/d/record', 'POST', details);
            console.log(result);
            setLoading(false);
            if (result && result.error) return setLoggeedMessage({ error: result.error });

            // Filtering should be based in combo form
            //region_code = param.region_code;
            //province_code = param.province_code;   
            //city_code = param.city;       

            // Filtering should be based in combo form         
            getHandler_refreshUser(region_code, province_code, city_code, district_code, barangay_code)  
            toast.info('Successfully deleted!', {
                position: "top-right",     theme: "colored",      autoClose: 500,
                hideProgressBar: true,      closeOnClick: true, pauseOnHover: true,
                draggable: true,            progress: '',
            });
        } catch (e) {

            setLoading(false);
            setLoggeedMessage({ error: e.message })

        }
        // exitEditingMode(); //required to exit editing mode
    }

    let title = param.region_label + "/" + param.province_label + "/" + param.city_label + "/" + param.district_label + "/" + param.barangay_label ;
    let sub_title = "/Users"

    // setState(true);
    return (
        <>
            <div style={state ? { pointerEvents: "auto", opacity: "1" } : { pointerEvents: "none", opacity: "0.4" }} >
                {/* <div>{title} = {state ? 'Add' : 'Cancel'}</div> */}
                <div style={{ fontSize: "14pt", fontWeight: "bold", display:'inline-block' }}>
                    {title}
                </div>
                <div style={{ fontSize: "14pt", fontWeight: "bold", display:'inline-block', color:'blue' }}>
                    {sub_title}
                </div>      
                <br />

                <ToastContainer
                    position="top-center"
                    theme="light"
                    autoClose={3000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />

                <Backdrop
                    sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
                    open={loadingIndicator}
                >
                    <CircularProgress color="inherit" />
                </Backdrop>

                {/*
                <div style={{ zIndex: '99999 !important' }}>
                    <Select
                        defaultValue={selectedOption}
                        onChange={setSelectedOption}
                        options={options}
                        styles={selectStyles}
                    />
                </div>

                <MaterialReactTable
                   options={{
                        maxBodyHeight: 400,
                        headerStyle: {
                            zIndex: 8
                        }
                    }}

                */ }
                
                <MaterialReactTable
                
                    columns={columns}
                    data={rows}
                    // data={data}
                    title='LIST OF DISTRICTS'
                    editingMode='modal'
                    initialState={{ columnVisibility: { region: false, province: false, city: false, district: false, barangay: false } }}
                    enableEditing={true}
                    onEditingRowSave={editHandler}
                    options={{
                        padding: "dense",
                    }}
                    // initialState={{ columnVisibility: { region: false } }}
                    // icons={tableHook}
                    //displayColumnDefOptions={{
                    //      enableHiding: true, //now row numbers are hidable too
                    //}}  
                    renderRowActions={({ row, table }) => (
                        <Box sx={{ display: 'flex', gap: '1rem' }}>
                            <Tooltip arrow placement="left" title="Edit">
                                <IconButton onClick={() => {
                                            table.setEditingRow(row)
                                            setRegionOld(row.original.region);
                                            setProvinceOld(row.original.province);
                                            setCityOld(row.original.city);
                                            setDistrictOld(row.original.district);
                                            setBarangayOld(row.original.barangay);
                                            setEmailOld(row.original.email);
                                        }
                                    }>
                                    <Edit />
                                </IconButton>
                            </Tooltip>
                            <Tooltip arrow placement="right" title="Delete">
                                <IconButton color="error" onClick={() => handleDeleteRow(row)}>
                                    <Delete />
                                </IconButton>
                            </Tooltip>
                        </Box>
                    )}

                />
            </div>
        </>
    )
}

export default table
import React, { useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { toast } from 'react-toastify';

// functions
import { logout } from '../api/user';

const Header = () => {
  const location = useLocation();
  const [isLoggedIn, setIsLoggedIn] = useState(false); // Track user login status

  const handleLogout = async (e) => {
    e.preventDefault();

    try {
      const res = await logout();
      toast.success(res.message);
      // Update login status to false and redirect the user to login
      setIsLoggedIn(false);
      window.location.href = '/'; // Use window.location.href to force a full page reload
    } catch (err) {
      console.error(err);
    }
  };

  // Check if the current route is "/home"
  const isHomePage = location.pathname === '/home';

  // Conditionally set login status based on the user's route
  const updateLoginStatus = () => {
    if (location.pathname === '/') {
      setIsLoggedIn(false);
    } else {
      setIsLoggedIn(true);
    }
  };

  // Update login status on component mount and route change
  React.useEffect(() => {
    updateLoginStatus();
  }, [location]);

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <Link className="navbar-brand" to="/">
        Jed
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav ml-auto">
          {/* {!isHomePage && ( // Conditionally render Signup link only when not on the home page
            <li className="nav-item">
              <Link className="nav-link" to="/signup">
                Signup
              </Link>
            </li>
          )} */}
          {!isLoggedIn && ( // Conditionally render Login link only when not logged in
            <li className="nav-item ">
              <Link className="nav-link" to="/">
                Login
              </Link>
            </li>
          )}
          {isLoggedIn && isHomePage && ( // Conditionally render Logout link only on the "/home" page when logged in
            <li className="nav-item ">
              <span
                className="nav-link"
                style={{ cursor: 'pointer' }}
                onClick={handleLogout}
              >
                Logout
              </span>
            </li>
          )}
        </ul>
      </div>
    </nav>
  );
};

export default Header;

const express = require('express')
const router = express.Router()

// import controllers
const { register, login, logout, getLoggedInUser } = require('../controllers/user')
const { get_info,add_info,update_info,delete_info } = require('../controllers/personalInfo')

// import middlewares
const { userRegisterValidator, userById } = require('../middlewares/user')
const { verifyToken } = require('../middlewares/auth')

// api routes
router.post('/register', register)
router.post('/login', login)
router.get('/logout', logout)
router.get('/user', verifyToken, userById, getLoggedInUser)

router.get('/g/info', get_info)
router.post('/a/info', add_info)
router.post('/u/info', update_info)
router.post('/d/info', delete_info)

module.exports = router;
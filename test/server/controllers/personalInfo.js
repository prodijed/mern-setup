const Personal = require('../models/peronalInfo')
const jwt = require('jsonwebtoken')
createError = require('http-errors')
require('dotenv').config()

module.exports.get_info = async (req, res) => {

    try {
        const x = await Personal.find({})
            .select('name age address')
        if (!x) throw createError(403, 'Not found!');
        res.send(x)
    }
    catch (e) {
        console.log(e)
    }
}

module.exports.add_info = async (req, res) => {

    try {
        const name = req.body.name
        const age = req.body.age
        const address = req.body.address
        const gender = req.body.gender

        const details = {
            name: name,
            age: age,
            address: address,
            address: gender,
        }
        const personal = new Personal(details);
        await personal.save();

        res.status(201).json({
            message: 'New Data Added'
        });
    }
    catch (e) {
        console.log(e)
    }
}

module.exports.update_info = async (req, res) => {

    try {
        const name = req.body.name
        const age = req.body.age
        const address = req.body.address
        const gender = req.body.gender
        const id = req.body.id

        const x = await Personal.findOne({ _id: id });
        if (!x) throw createError(403, `Report not found!`);
        x.name = name;
        x.age = age;
        x.address = address;
        x.gender = gender;
        x.save();

        res.send({ success: 'Info Successfully updated' })
    }
    catch (e) {
        console.log(e)
    }
}

module.exports.delete_info = async (req, res) => {

    const { id } = req.body
    Personal.findByIdAndDelete(id)
        .then(() => {
            res.send({ success: "Delete Successfully" })
        })
        .catch((err) => {
            console.log(err)
        })
}
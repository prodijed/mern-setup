const mongoose = require('mongoose')
const uuvidv1 = require('uuidv1')
const crypto = require('crypto')

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        require: true,
        trim: true,
        unique: true,
        lowercase: true
    },
    email: {
        type: String,
        require: true,
        trim: true,
        unique: true,
        lowercase: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: String,
},
    {
        timestamps: true,
    });

// Virtual Field
userSchema.virtual('password').set(function (password) {
    // Create temp variable called _password
    this._password = password;

    //Generate a timestamp, uuidv1 gices us the unix timestamp
    this.salt = uuvidv1();

    //Encrypt the password function call
    this.hashedPassword = this.encryptPassword(password);
});

// Methods
userSchema.methods = {
    encryptPassword: function (password) {
        if (!password) return '';
        try {
            return crypto.createHmac('sha256', this.salt)
                .update(password)
                .digest('hex');
        }
        catch (err) {
            return '';
        }
    },
    authenticate: function (plainText) {
        return this.encryptPassword(plainText) === this.hashedPassword;
    }
}

module.exports = mongoose.model('User', userSchema);
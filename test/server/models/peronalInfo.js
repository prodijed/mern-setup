const mongoose = require('mongoose')
const uuvidv1 = require('uuidv1')
const crypto = require('crypto')

const personalInfoSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    age: {
        type: String,
    },
    address: {
        type: String,
    },
    gender: {
        type: String,
    }
});

module.exports = mongoose.model('peronalinfolist', personalInfoSchema);
import * as React from 'react';
import Form from './Form'
import Table from './Table';
function Main() {

  let holder = (datainfo) => { };
  const setData = (data) => {
    holder && holder(data);
  };
  const getData = (handler) => {
    holder = handler;
  };

  let commands = (datainfo) => { }
  const setCommand = (data) => {
    commands && commands(data)
  }
  const getCommand = (handler) => {
    commands = handler
  }

  return (
    <div className='container'>
      <div className="row" style={{ marginTop: '6rem' }}>
        <div className="col-10">
          <Table getData={getData} setCommand={setCommand} />
        </div>
        <div className="col-2">
          <Form setData={setData} getCommand={getCommand} />
        </div>
      </div>
    </div>
  )
}

export default Main
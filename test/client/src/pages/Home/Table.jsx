import * as React from 'react';
import { MaterialReactTable, } from 'material-react-table';
import { Box, IconButton, Tooltip, Button } from '@mui/material';
import { Delete, Edit } from '@mui/icons-material';
// Import Hook
import { useFetch } from '../../hook/Http_Hook';

const Table = (props) => {

    const [data, setData] = React.useState()
    const [columns, setColumns] = React.useState([])
    const [rows, setRows] = React.useState([])
    const [rowSelection, setRowSelection] = React.useState([])
    const { sendRequest } = useFetch()

    const dataLoader = async () => {
        try {
            const result = await sendRequest('/g/info', 'GET')
            if (result && result.error) return setLogMessage({ error: result.error })
            setData(result)
        }
        catch (error) {
            console.log(error)
        }
    }

    React.useEffect(() => {
        dataLoader()
    }, [])

    function tableDataLoader() {
        try {
            const column_holder = [
                { header: 'ID', accessorKey: '_id', enableEditing: false },
                { header: 'Name', accessorKey: 'name', enableEditing: false },
                { header: 'Age', accessorKey: 'age', enableColumnActions: false, },
                { header: 'Address', accessorKey: 'address', enableColumnActions: false, },
            ]

            let row = []
            data.forEach(x => {
                row.push({
                    _id: x._id,
                    name: x.name,
                    age: x.age,
                    address: x.address,
                })
            })
            setColumns(column_holder)
            setRows(row)
        }
        catch (error) {
            console.log(error)
        }
    }

    React.useEffect(() => {
        if (!data || data.length === 0) return
        tableDataLoader()
    }, [data])

    const [getParam, setParam] = React.useState('refresh')
    const create_info = async () => {
        const param = 'create'
        props.setCommand(param)
        setParam(param)
        setRowSelection([])
    }

    let button;
    const refresh_info = async () => {
        const param = 'refresh'
        props.setCommand(param)
        setParam(param)
        dataLoader()
        tableDataLoader()
        setRowSelection([])
    }

    const save_info = async () => {
        const param = 'save'
        props.setCommand(param)
        setParam(param)
    }

    const edit_info = async () => {
        const param = 'edit'
        props.setCommand(param)
        setParam(param)
    }

    const update_info = async () => {
        const param = 'update'
        props.setCommand(param)
        setParam(param)
    }

    const [buttonStatus, setButtonStatus] = React.useState(true);
    const container = (param) => {
        if (param === 'Empty') {
            setButtonStatus(true)
        }
        else if (param == 'refresh') {
            refresh_info()
        }
        else {
            setButtonStatus(false)
        }
    }
    props.getData(container)

    if (getParam === 'refresh') {
        button = <Button variant='contained' onClick={create_info}> Create </Button>
    }
    else if (getParam === 'create') {
        buttonStatus
            ? button = <Button variant='contained' onClick={save_info} disabled> Save </Button>
            : button = <Button variant='contained' onClick={save_info}> Save </Button>
    }
    else {
        button = <Button variant='contained' onClick={update_info}> Update </Button>
    }

    const func_Checkbox_SetGetSelected_Row = async (row) => {
        if (JSON.stringify(row).length !== 0) {
            props.setCommand({
                action: 'loading',
                name: row.original.name,
                age: row.original.age,
                address: row.original.address,
                id: row.original._id
            });
        }
    }

    const deleteData = async (row) => {
        try {
            const details = {
                id: row.getValue('_id')
            }
            if (!window.confirm(`Are you sure you want to delete ${row.getValue('name')}`)) {
                console.log('Return delete..')
                return;
            }
            else {
                const result = await sendRequest('/d/info', 'POST', details)
                let ctr = 0
                if (result && result.error) {
                    ctr = 1;
                    alert('Error Found: ' + result)
                }

                if (ctr === 0) {
                    alert('Data deleted')
                    refresh_info()
                }
            }
        }
        catch (error) {
            alert('Error: ' + error)
        }
    }

    return (
        <>
            {button}
            <Button variant='contained' color='warning' style={{ marginLeft: '10px' }}
                onClick={() => refresh_info()}> Refresh </Button>
            <MaterialReactTable
                columns={columns}
                data={rows}
                enableEditing={true}
                renderRowActions={({ row, table }) => (
                    <Box sx={{ display: 'flex', gap: '1rem' }}>
                        <Tooltip arrow placement="left" title="Edit">
                            <IconButton onClick={() => { edit_info() }} >
                                <Edit />
                            </IconButton>
                        </Tooltip>
                        <Tooltip arrow placement="right" title="Delete">
                            <IconButton color="error" onClick={() => deleteData(row)}>
                                <Delete />
                            </IconButton>
                        </Tooltip>
                    </Box>
                )}

                enableMultiRowSelection={false}
                enableRowSelection={true}
                muiTableBodyRowProps={({ row }) => ({
                    onClick: () => {
                        setRowSelection(() => ({
                            [row.id]: [row.id],
                        }));
                        func_Checkbox_SetGetSelected_Row(row)
                    },
                    sx: { cursor: 'pointer' },
                })}

                initialState={{
                    pagination: { pageSize: 100, pageIndex: 0 },
                    density: 'compact',
                    columnVisibility: {
                        _id: false,
                    },
                }}

                onRowSelectionChange={setRowSelection}
                state={{ rowSelection }}
                muiSelectCheckboxProps={({ row }) => ({
                    onClick: (event) => {
                        func_Checkbox_SetGetSelected_Row(row);
                    },
                    sx: { borderRight: '1px solid #e0e0e0' },
                })}
                muiTableContainerProps={{ sx: { maxHeight: '500px' } }}
            />
        </>
    )
}

export default Table
import * as React from 'react';
import { TextField, Button } from '@mui/material';
// Import Hook
import { useFetch } from '../../hook/Http_Hook';

const Form = (props) => {
  const { sendRequest } = useFetch()
  const [commandAction, setCommandAction] = React.useState(true);
  const cmd = (param) => {
    if (param === 'refresh') {
      setCommandAction(true)
      clearText()
    }
    else if (param === 'create') {
      setCommandAction(false)
      clearText()
    }
    else if (param === 'save') {
      saveData()
    }
    else if (param === 'edit') {
      setCommandAction(false)
    }
    else if (param.action === 'loading') {
      clearText()
      setInfo({
        ...getInfo,
        name: param.name,
        age: param.age,
        address: param.address,
        id: param.id
      })
    }
    else if (param === 'update') {
      updateData()
      setCommandAction(false)
      clearText()
      props.setData('Refresh')
    }
  }
  props.getCommand(cmd)

  const [getInfo, setInfo] = React.useState({
    name: '',
    age: '',
    address: ''
  })

  function clearText() {
    setInfo({
      name: '',
      age: '',
      address: ''
    })
  }

  //Check if fields are empty to trigger button enable/disable
  if (!getInfo.name || !getInfo.age || !getInfo.address) {
    props.setData('Empty')
  }
  else {
    props.setData('Full')
  }

  const saveData = async () => {
    try {
      const details = {
        name: getInfo.name,
        age: getInfo.age,
        address: getInfo.address
      }
      const result = await sendRequest('/a/info', 'POST', details)
      let ctr = 0
      if (result && result.error) {
        ctr = 1;
        alert('Error Found: ' + result)
      }

      if (ctr === 0) {
        alert('New data added')
        setCommandAction(true)
        props.setData('refresh')
      }
    }
    catch (error) {
      alert('Error: ' + error)
    }
  }

  const updateData = async () => {
    try {
      const details = {
        name: getInfo.name,
        age: getInfo.age,
        address: getInfo.address,
        id: getInfo.id
      }
      const result = await sendRequest('/u/info', 'POST', details)
      let ctr = 0
      if (result && result.error) {
        ctr = 1;
        alert('Error Found: ' + result)
      }

      if (ctr === 0) {
        alert('Data updated')
        setCommandAction(true)
        props.setData('refresh')
      }
    }
    catch (error) {
      alert('Error: ' + error)
    }
  }


  return (
    <>
      <div className='mt-3'>
        <TextField
          size='small'
          variant='outlined'
          className='form-control'
          label='Name'
          value={getInfo.name}
          onChange={(e) => {
            setInfo({
              ...getInfo,
              name: e.target.value
            })
          }}
          disabled={commandAction}
          style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
        />
        <div className='mt-2'>
          <TextField
            size='small'
            variant='outlined'
            className='form-control'
            label='Age'
            value={getInfo.age}
            onChange={(e) => {
              setInfo({
                ...getInfo,
                age: e.target.value
              })
            }}
            disabled={commandAction}
            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
          />
        </div>
        <div className='mt-2'>
          <TextField
            size='small'
            variant='outlined'
            className='form-control'
            label='Address'
            value={getInfo.address}
            onChange={(e) => {
              setInfo({
                ...getInfo,
                address: e.target.value
              })
            }}
            disabled={commandAction}
            style={commandAction ? { backgroundColor: 'lightgrey' } : { backgroundColor: '' }}
          />
        </div>
      </div>
    </>
  )
}

export default Form
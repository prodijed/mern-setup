import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
// Design
import {
    TextField, InputAdornment, IconButton, OutlinedInput, FormControl, InputLabel, Button,
    Alert, Stack
} from '@mui/material';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';


// Import Hook
import { useFetch } from '../hook/Http_Hook';

function Login() {

    const [getEmail, setEmail] = useState('')
    const [getPassword, setPassword] = useState('')
    const [showPassword, setShowPassword] = useState(true)
    //Usage of Hook
    const { sendRequest } = useFetch()

    const history = useNavigate()
    const loginAccount = async () => {
        try {
            const info = {
                email: getEmail,
                hashedPassword: getPassword,
            }

            const result = await sendRequest('/login', 'POST', info)

            let ctr = 0;
            if (result && result.error) {
                ctr = 1;
                alert('Error Found: ' + result.error)
            }

            if (ctr === 0) {
                alert('Successfully Login')
                history('/home')
            }
        }
        catch (error) {
          alert(error)
        }
    }

    return (
        <>
            <div className="container mt-5 mb-5 col-10 col-small-8 col-md-6 col-lg-5" style={{ border: "1.5px solid black", padding: "15px"  }}>
                <div className="text-center mb-5 alert alert-primary">
                    <label className='h2'>
                        Login
                    </label>
                </div>
                <div className='form-group'>
                    <TextField
                        size='small'
                        variant='outlined'
                        className='form-control'
                        label='Email'
                        value={getEmail}
                        onChange={(e) => { setEmail(e.target.value) }}
                    />
                </div>
                <div className='form-group mt-2'>
                    <FormControl
                        size='small'
                        variant='outlined'
                        className='form-control'>
                        <InputLabel>Password</InputLabel>
                        <OutlinedInput
                            label='Password'
                            type={showPassword ? ('password') : ('text')}
                            value={getPassword}
                            onChange={(e) => { setPassword(e.target.value) }}
                            endAdornment={
                                <InputAdornment>
                                    <IconButton edge='end' onClick={() => { setShowPassword(!showPassword) }}>
                                        {showPassword ? (<VisibilityIcon />) : (<VisibilityOffIcon />)}
                                    </IconButton>
                                </InputAdornment>
                            } />
                    </FormControl>
                </div>
                <div className='text-center mt-4'>
                    <Button variant='contained' disabled={!getEmail || !getPassword} onClick={loginAccount}>
                        Login
                    </Button>
                    <Button style={{ marginLeft: '5px' }} variant='contained' color='info' onClick={() => { history('/signup') }}>
                        Signup
                    </Button>
                </div>
            </div>
        </>
    )
}

export default Login
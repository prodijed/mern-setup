import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
// Design
import {
    TextField, InputAdornment, IconButton, OutlinedInput, FormControl, InputLabel, Button,
    FormHelperText, Alert, Stack
} from '@mui/material';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';

// Import Hook
import { useFetch } from '../hook/Http_Hook';

function Signup() {

    const [getUsername, setUsername] = useState('')
    const [getEmail, setEmail] = useState('')
    const [getPassword, setPassword] = useState('')
    const [getConfirmPassword, setConfirmPassword] = useState('')
    const [showPassword, setShowPassword] = useState(true)

    // Password validation
    let hasSixChar = getPassword.length >= 6
    let hasLowerChar = /(.*[a-z].*)/.test(getPassword)
    let hasUpperChar = /(.*[A-Z].*)/.test(getPassword)
    let hasNumberChar = /(.*[0-9].*)/.test(getPassword)
    let hasSpecialChar = /(.*[^a-zA-Z0-9].*)/.test(getPassword)

    //Usage of Hook
    const { sendRequest } = useFetch()

    const history = useNavigate()
    const addAccount = async () => {
        try {
            const info = {
                username: getUsername,
                email: getEmail,
                hashedPassword: getPassword,
            }

            const result = await sendRequest('/register', 'POST', info)

            let ctr = 0;
            if (result && result.error) {
                ctr = 1;
                alert('Error Found: ' + result.error)
            }

            if (ctr === 0) {
                alert('Successfully Registered')
                history('/')
            }
        }
        catch (error) {
          alert(error)
        }
    }

    return (
        <>
            <div className="container mt-5 mb-5 col-10 col-small-8 col-md-6 col-lg-5" style={{ border: "1.5px solid black", padding: "15px"  }}>
                <div className="text-center mb-5 alert alert-primary">
                    <label className='h2'>
                        Sign Up
                    </label>
                </div>
                <div className='form-group'>
                    <TextField
                        size='small'
                        variant='outlined'
                        className='form-control'
                        label='Username'
                        value={getUsername}
                        onChange={(e) => { setUsername(e.target.value) }}
                    />
                </div>
                <div className='form-group mt-2'>
                    <TextField
                        size='small'
                        variant='outlined'
                        className='form-control'
                        label='Email'
                        value={getEmail}
                        onChange={(e) => { setEmail(e.target.value) }}
                    />
                </div>
                <div className='form-group mt-2'>
                    <FormControl
                        size='small'
                        variant='outlined'
                        className='form-control'>
                        <InputLabel>Password</InputLabel>
                        <OutlinedInput
                            label='Password'
                            type={showPassword ? ('password') : ('text')}
                            value={getPassword}
                            onChange={(e) => { setPassword(e.target.value) }}
                            endAdornment={
                                <InputAdornment>
                                    <IconButton edge='end' onClick={() => { setShowPassword(!showPassword) }}>
                                        {showPassword ? (<VisibilityIcon />) : (<VisibilityOffIcon />)}
                                    </IconButton>
                                </InputAdornment>
                            } />
                    </FormControl>
                    {getPassword && (
                        <div className='ml-1' style={{ columns: 2 }}>
                            <div>
                                <small className={hasSixChar ? 'text-success' : 'text-danger'}>
                                    at least 6 characters
                                </small>
                            </div>
                            <div>
                                <small className={hasLowerChar ? 'text-success' : 'text-danger'}>
                                    at least 1 lowercase letter
                                </small>
                            </div>
                            <div>
                                <small className={hasUpperChar ? 'text-success' : 'text-danger'}>
                                    at least 1 uppercase letter
                                </small>
                            </div>
                            <div>
                                <small className={hasNumberChar ? 'text-success' : 'text-danger'}>
                                    one number
                                </small>
                            </div>
                            <div>
                                <small className={hasSpecialChar ? 'text-success' : 'text-danger'}>
                                    one special symbol
                                </small>
                            </div>
                        </div>)}
                </div>
                <div className='form-group mt-2'>
                    <TextField
                        size='small'
                        variant='outlined'
                        className='form-control'
                        label='Confirm Password'
                        type='password'
                        value={getConfirmPassword}
                        onChange={(e) => { setConfirmPassword(e.target.value) }}
                    />
                    {getPassword && getConfirmPassword && (<FormHelperText className='ml-1 mt-1'>
                        {getPassword === getConfirmPassword ?
                            (<span className='text-success'>Password does match</span>) :
                            (<span className='text-danger'>Password does not match</span>)}
                    </FormHelperText>)}
                </div>
                <div className='text-center mt-4'>
                    <Button variant='contained' disabled={
                        !getUsername ||
                        !getEmail ||
                        !getPassword ||
                        !getConfirmPassword ||
                        getPassword !== getConfirmPassword ||
                        !hasSixChar ||
                        !hasLowerChar ||
                        !hasUpperChar ||
                        !hasNumberChar ||
                        !hasSpecialChar
                    }
                        onClick={addAccount}>
                        Signup
                    </Button>
                    <Button style={{ marginLeft: '5px' }} variant='contained' color='info' onClick={() => { history('/') }}>
                        Login
                    </Button>
                </div>
            </div >
        </>
    )
}

export default Signup